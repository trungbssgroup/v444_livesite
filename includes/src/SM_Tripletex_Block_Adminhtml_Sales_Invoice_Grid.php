<?php
/**
 * Tripletex Integration
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at magento@smartosc.com or
 * visit us at http://smartosc.com if you have any questions about this.
 * SM is not responsible for any problems caused by this file.
 *
 * Visit us at http://smartosc.com today!
 *
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @copyright  Copyright (c) 2012 SmartOSC (http://smartosc.com)
 * @license    Single-site License
 *
 */

class SM_Tripletex_Block_Adminhtml_Sales_Invoice_Grid extends Mage_Adminhtml_Block_Sales_Invoice_Grid
{
		protected function _prepareColumns()
    {
 				$this->addColumnAfter('tripletex_exported', array(
				            'header'    => Mage::helper('tripletex')->__('Tripeltex'),
										'index'			=> 'tripletex_exported',
				            'type'      => 'options',
										'options'   => Mage::helper('tripletex')->getStatuses(),
																												),
															'grand_total');

				return parent::_prepareColumns();
    }
}