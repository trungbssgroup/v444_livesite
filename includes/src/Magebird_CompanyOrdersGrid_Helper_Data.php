<?php
/**
 * Magebird.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send
 * an email to info@magebird.com so we can send you a copy immediately.
 *
 * @category   Magebird
 * @package    Magebird_Customersgrid
 * @copyright  Copyright (c) 2014 Magebird (http://www.Magebird.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Magebird_CompanyOrdersGrid_Helper_Data extends Mage_Core_Helper_Abstract
{
	
}