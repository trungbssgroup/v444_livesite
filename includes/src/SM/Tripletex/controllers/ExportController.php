<?php

class SM_Tripletex_ExportController extends Mage_Adminhtml_Controller_Action
{

  public function indexAction()
  {
    $ids = $this->getRequest()->getPost('invoice_ids');

    $tripletex = Mage::getModel('tripletex/cron');

    $completed = 0;
    $failed = 0;
    $skipped = 0;

    if (!empty($ids)) {
      foreach ($ids as $invoiceId)
      {

        $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);

        $store_id = $invoice->getStoreId();

        if (!Mage::getStoreConfig('tripletex/tripletex_settings/active',$store_id)) {
          $skipped++;
          continue;
    		}

        if ($invoice->getTripletexExported() > 0) {
          $skipped++;
          continue;
        }

	  		if ($tripletex->processInvoice($invoice)) {
	         $completed++;
	  		}
	  		else
	  		{
	         $failed++;
	  		}
      }
    }

    if ($completed > 0) {
      $this->_getSession()->addSuccess(Mage::helper('tripletex')->__('% s invoices were exported.',$completed));
    }

    if ($skipped > 0) {
      $this->_getSession()->addNotice(Mage::helper('tripletex')->__('% s invoices have already been exported before.',$skipped));
    }

    if ($failed > 0) {
      $this->_getSession()->addError(Mage::helper('tripletex')->__('% s failed during export invoices. Check the log files.',$failed));
    }

		Mage::getConfig()->cleanCache();

    $this->_redirect('adminhtml/sales_invoice');
  }
}