<?php

class Namespace_EmailQueueFix_Model_Resource_Email_Queue extends Mage_Core_Model_Resource_Email_Queue {
    /**
     * Remove already sent messages - ADDED CTH 04AUG2015 ref Magento StackExch 51928.
     * ADDED: also remove all recipients of sent messages!
     *
     * @return Mage_Core_Model_Resource_Email_Queue
     */
    public function removeSentMessages() {
        $writeAdapter = $this->_getWriteAdapter();
        $readAdapter = $this->_getReadAdapter();
        $select = $readAdapter->select()->from(array("ceqr" => $this->getTable('core/email_recipients')), array('*'))->joinLeft(array('ceq' => $this->getMainTable()), 'ceqr.message_id = ceq.message_id', array('*'))->where('ceq.processed_at IS NOT NULL OR ceq.message_id IS NULL');
        $recipients = $readAdapter->fetchAll($select);
        if ( $recipients ) {
            foreach ( $recipients as $recipient ) {
                $writeAdapter->delete($this->getTable('core/email_recipients'), "recipient_id = " . $recipient['recipient_id']);
            }
        }
        $writeAdapter->delete($this->getMainTable(), 'processed_at IS NOT NULL');
        return $this;
    }

}