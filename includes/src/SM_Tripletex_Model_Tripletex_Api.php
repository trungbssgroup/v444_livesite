<?php
/**
 * Tripletex Payment Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@smartosc.com so we can send you a copy immediately.
 *
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @author     longnv (longnv@smartosc.com)
 * @copyright  Copyright (c) 2011 SmartOSC. (http://www.smartosc.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SM_Tripletex_Model_Tripletex_Api
{
  private $sessionid;
  private $http;
  private $header;
  private $cookie;

  private $error;


  public function __construct()
  {
	$this->http = new Varien_Http_Adapter_Curl();
  }

  /**
     * API endpoint getter
     *
     * @return string
     */
    public function getApiEndpoint()
    {
        return $url = 'https://tripletex.no/JSON-RPC';
    }

	/**
	 * config getter
	 *
	 * return array
	 */
	public function getConfig($timeout) {
		return array('timeout' => $timeout);
	}

  public function setLogin($username,$password) {
	$data = Mage::getStoreConfig('tripletex');
	$syncsystem = $data['tripletex_settings']['syncsystem'];
	$syncpassword = $data['tripletex_settings']['syncpassword'];
    $result = false;
    $this->data = array();
    $this->data['method'] = "Sync.login";
	$this->data['params'][0] = $syncsystem;
    $this->data['params'][1] = $syncpassword;
    $this->data['params'][2] = $username;
    $this->data['params'][3] = $password;
    $this->data['id'] = '1';
	$this->headers = array("Content-Type: application/json");
	$this->http->setConfig($this->getConfig(30));

	$response = $this->docurl();

    $request_ok = $this->check_result($response);
    if (!$request_ok) {
      $this->sessionid = NULL;
    } else {
		$this->setSessionId($response);
	}

    return $request_ok;
  }

  public function logout() {
	$this->setHeaders();

    $this->data = array();
    $this->data['method'] = "Sync.logout";
    $this->data['params'] = array('');
    $this->data['id'] = '1';

    $this->docurl();
  }

  public function getSessionId()
  {
    return $this->sessionid;
  }

  private function setSessionId($curlHeader) {
    $cookie_arr = explode(';',substr($curlHeader,11,-1));
    foreach ($cookie_arr as $fullcookie) {
		$cookie = explode("=",trim($fullcookie));
		$pos = strpos($cookie[0], "Set-Cookie: JSESSIONID");
		if ($pos) {
			$this->sessionid = $cookie[1];
			break;
		}
    }
  }

  private function setCookie() {
    if ($this->sessionid != null) {
		$this->cookie = "Cookie: JSESSIONID=" . $this->sessionid;
    }
    else {
		$this->cookie = "";
    }
  }

  public function setHeaders() {
	$this->setCookie();
	if ($this->cookie != null && $this->cookie != "") {
		$this->headers = array($this->cookie, "Content-Type: application/json");
	} else {
		$this->headers = array("Content-Type: application/json");
	}
  }

  public function importInvoice($csvdata) {
	$this->setHeaders();

    $this->data = array();
    $this->data['method'] = 'Invoice.importInvoicesTripletexCSV';
    $this->data['params'] = array($csvdata,'ISO-8859-1',true,false);
    $this->data['id'] = 1;

    return $this->check_result($this->docurl());
  }

  public function createPaymentVoucher($invoiceNo, $date, $payment, $amount)
  {
	$this->setHeaders();

    $this->data = array();
    $this->data['method'] = 'Invoice.createPaymentVoucher';
    $this->data['params'] = array($invoiceNo,$date,$payment,$amount);
    $this->data['id'] = 1;

    return $this->check_result($this->docurl());
  }

  public function getHistoryAmountCurrencyOutstanding($invoiceNo) {
	$this->setHeaders();

    $this->data = array();
    $this->data['method'] = 'Invoice.getHistoryAmountCurrencyOutstanding';
    $this->data['params'] = array($invoiceNo);
    $this->data['id'] = 1;

    return $this->fetch_result($this->docurl());
  }

  public function getError()
  {
    return $this->error;
  }



  private function check_result($json_string) {
	$json_string = preg_split('/^\r?$/m', $json_string, 2);
    $json_string = trim($json_string[1]);
	$json = json_decode($json_string);

	if (is_null($json))  {
    	$result = false;
    	$this->error = "Not a valid Json String.";
    }
	else {
	    if (isset($json->error)) {
	      $result = false;
	      $this->error = $json->error->msg;
	    }
	    else {
	      $result = true;
	    }
    }
    return $result;
  }

  private function fetch_result($json_string) {
    $json = json_decode($json_string);
	var_dump($json);die;
    if (is_null($json))  {
    	$result = false;
    	$this->error = "Not a valid Json String.";
    }
    else {
	    if (isset($json->error)) {
	      $result = false;
	      $this->error = $json->error->msg;
	    }
	    else {
	      $result = $json->result;
	    }
    }
    return $result;
  }

  private function docurl() {
	$this->http->write(Zend_Http_Client::POST, $this->getApiEndpoint(), '1.1', $this->headers, json_encode($this->data));
	$response = $this->http->read();

	return $response;
  }
}