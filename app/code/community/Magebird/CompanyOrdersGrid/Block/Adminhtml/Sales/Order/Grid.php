<?php

/**
 * Magebird.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send
 * an email to info@magebird.com so we can send you a copy immediately.
 *
 * @category   Magebird
 * @package    Magebird_Customersgrid
 * @copyright  Copyright (c) 2014 Magebird (http://www.Magebird.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
class Magebird_CompanyOrdersGrid_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{  
    
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        
        $tableName = Mage::getSingleton("core/resource")->getTableName('sales_flat_order_address');                  
        $collection->getSelect()->join($tableName, "main_table.entity_id = $tableName.parent_id AND $tableName.address_type='billing'",array('company'));
        $this->setCollection($collection);               
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }     


    protected function _prepareColumns()
    {
        $this->addColumnAfter('company', array(
            'header'    => Mage::helper('customer')->__('Company'),
            'index'     => 'company'
        ),'billing_name');   
              
        return parent::_prepareColumns();
    }

}
