<?php
/**
 * Tripletex Integration
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any 
 * part of it. Please contact us by email at magento@smartosc.com or 
 * visit us at http://smartosc.com if you have any questions about this.
 * SM is not responsible for any problems caused by this file.
 *
 * Visit us at http://smartosc.com today!
 * 
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @copyright  Copyright (c) 2009 SM (http://smartosc.com)
 * @license    Single-site License
 * 
 */

/**
 * Data helper
 */
class SM_Tripletex_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getStatuses()
	{
		return array('0' => $this->__('Do not exported'), 
								 '1' => $this->__('Exported'),
								);
	}
  
} 
