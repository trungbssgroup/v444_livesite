<?php
/**
 * Tripletex Integration
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at magento@smartosc.com or
 * visit us at http://smartosc.com if you have any questions about this.
 * SM is not responsible for any problems caused by this file.
 *
 * Visit us at http://smartosc.com today!
 *
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @copyright  Copyright (c) 2012 SmartOSC (http://smartosc.com)
 * @license    Single-site License
 *
 */

class SM_Tripletex_Model_Config_Backend_Cronimportinterval
{

    public function toOptionArray()
    {
        $intervals = array(
            array(
                'label' => Mage::helper('tripletex')->__('Daily (Runs 23:00)'),
                'value' => 'daily'
            ),
            array(
                'label' => Mage::helper('tripletex')->__('Every 12 hours (running 1:00 and 13:00)'),
                'value' => '12hour'
            ),
            array(
                'label' => Mage::helper('tripletex')->__('Every hour'),
                'value' => 'hourly'
            ),
                          );

        return $intervals;
    }
}