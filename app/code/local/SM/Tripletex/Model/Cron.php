<?php
/**
 * Tripletex Integration
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at magento@smartosc.com or
 * visit us at http://smartosc.com if you have any questions about this.
 * SM is not responsible for any problems caused by this file.
 *
 * Visit us at http://smartosc.com today!
 *
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @copyright  Copyright (c) 2012 SmartOSC (http://smartosc.com)
 * @license    Single-site License
 *
 */
class SM_Tripletex_Model_Cron
{

  protected $_tripletex;

	public function tripletex_export($scheduler)
	{
    	$stores = Mage::app()->getStores();
    	foreach ($stores as $store) {

    		if (!Mage::getStoreConfig('tripletex/tripletex_settings/active',$store->getId())) {
    			continue;
    		}

	      $collection = Mage::getModel('sales/order_invoice')->getCollection()
	            ->addFieldToSelect('*')
	            ->addAttributeToFilter('tripletex_exported',0)
	            ->addAttributeToFilter('store_id',$store->getId());

	      $start_invoice = Mage::getStoreconfig('tripletex/tripletex_settings/start_invoice',$store->getId());

	      if ((int)$start_invoice > 0) {
	        $collection->addFieldToFilter('increment_id',array("gteq" => $start_invoice));
	      }


	  		foreach ($collection->getItems() as $invoice) {
	  		  $this->processInvoice($invoice);
	  		}

	  		Mage::getConfig()->cleanCache();
    	}
	}

	public function processInvoice($invoice) {
    $tripletex = $this->getTripletex();

    $tripletex->addInvoice($invoice);
    $export_ok = false;
  	if (Mage::getStoreconfig('tripletex/tripletex_settings/testmode',$invoice->getStoreId())) {
  		if ($tripletex->saveCsv(Mage::getBaseDir('log').'/tripletex.csv')) {
  			$export_ok = true;
  		}
  	}
  	else {
  		if ($tripletex->send()) {
  			$export_ok = true;
        $invoice->setTripletexExported(1);
  	    $invoice->save();
  		}
  	}

  	if ($export_ok) {
      $tripletex->log("Exporting invoice #".$invoice->getIncrementId()." successfull.");
  	}
  	else
  	{
      $tripletex->log("Exporting invoice #".$invoice->getIncrementId()." failed.");
  	}

  	return $export_ok;
	}

	public function checkInvoiceStatus($scheduler) {
	    $tripletex = Mage::getModel('tripletex/tripletex_invoice');

    	$stores = Mage::app()->getStores();
    	foreach ($stores as $store) {
    		if (!Mage::getStoreConfig('tripletex/tripletex_settings/active',$store->getId())) {
    			continue;
    		}

    		if (!$tripletex->isAuthed()) {
    		  // Skip this store if we are not able to login.
    		  continue;
    		}

	      $collection = Mage::getModel('sales/order_invoice')->getCollection()
	            ->addFieldToSelect('*')
	            ->addAttributeToFilter('state',Mage_Sales_Model_Order_Invoice::STATE_OPEN)
	            ->addAttributeToFilter('store_id',$store->getId());

	      $start_invoice = Mage::getStoreconfig('tripletex/tripletex_settings/start_invoice',$store->getId());

	      if ((int)$start_invoice > 0) {
	        $collection->addFieldToFilter('increment_id',array("gteq" => $start_invoice));
	      }

	  		foreach ($collection->getItems() as $invoice) {
	  		  if ($tripletex->getOutstandingInvoiceAmount($invoice->getIncrementId()) <= 0) {
	  		    if ($invoice->canCapture()) {
	  		      $tripletex->log('Faktura '.$invoice->getIncrementId().' er ferdig oppgjort i Tripletex');
  	  		    $invoice->capture();
              $invoice->getOrder()->setIsInProcess(true);
              $transactionSave = Mage::getModel('core/resource_transaction')
                  ->addObject($invoice)
                  ->addObject($invoice->getOrder())
                  ->save();
	  		    }
	  		  }
	  		}
    	}
	}

	protected function getTripletex() {
	  if (!isset($this->_tripletex)) {
	    $this->_tripletex = Mage::getModel('tripletex/tripletex_invoice');
	  }

	  return $this->_tripletex;
	}

}