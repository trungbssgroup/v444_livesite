<?php
class SM_Tripletex_Model_Observer
{

  public function addMassAction($observer) {
    $data = $observer->getEvent()->getBlock()->getData();
    if (isset($data['type']) &&
        $data['type'] == 'adminhtml/widget_grid_massaction' &&
        Mage::app()->getRequest()->getControllerName() === 'sales_invoice'
       ) {
        $observer->getEvent()->getBlock()->addItem('tripletex', array(
            'label'=> Mage::helper('sales')->__('Export to Tripletex'),
            'url' => Mage::app()->getStore()->getUrl('tripletex/export'),
        ));
       }
  }
}