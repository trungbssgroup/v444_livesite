<?php
/**
 * Tripletex Integration
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at magento@smartosc.com or
 * visit us at http://smartosc.com if you have any questions about this.
 * SM is not responsible for any problems caused by this file.
 *
 * Visit us at http://smartosc.com today!
 *
 * @category   Payments & Gateways Extensions
 * @package    SM_Tripletex
 * @copyright  Copyright (c) 2012 SmartOSC (http://smartosc.com)
 * @license    Single-site License
 *
 */
class SM_Tripletex_Block_Adminhtml_Form_Field_Methodselect extends Mage_Core_Block_Html_Select
{
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {

            $store = $this->getRequest()->getParam('store');
            $storeId = false;
            if ($store)
            {
              $storeId = (int)Mage::getConfig()->getNode('stores/' . $store . '/system/store/id');
            }

            $methods = Mage::helper('payment')->getPaymentMethodList(true, true); // Mage::getSingleton('payment/config')->getAllMethods();
            $activeMethods = array_keys(Mage::getSingleton('payment/config')->getActiveMethods($storeId));
            foreach ($methods as $method => $title)
            {
              if (in_array($method,$activeMethods)) { $title['label'] .= '*'; }
              $this->addOption($method,$title['label']);
            }
            /*foreach ($methods as $method => $data) {
              if ($data->getTitle()) {
                $title = $data->getTitle();
                if (in_array($method,$activeMethods)) { $title .= '*'; }
                $this->addOption($method,$title);
              }
            }
            */
        }
        return parent::_toHtml();
    }
}